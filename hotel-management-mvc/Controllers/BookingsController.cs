﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using hotel_management_mvc.Models;
using Newtonsoft.Json;

namespace hotel_management_mvc.Controllers
{
    public class BookingsController : Controller
    {
        private readonly HotelManagementContext _context;
        public BookingsController(HotelManagementContext context)
        {
            _context = context;
        }

        // GET: Bookings
        public async Task<IActionResult> Index(string searchString)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            var bookings = await _context.Bookings.OrderByDescending(x => x.Id).ToListAsync();
            if (!string.IsNullOrEmpty(searchString))
            {
                bookings = bookings.Where(x => x.CustomerName.Trim().ToLower().Contains(searchString.Trim().ToLower()) || x.Checkin.ToString().Trim().ToLower().Contains(searchString.Trim().ToLower()) || x.Checkout.ToString().Trim().ToLower().Contains(searchString.Trim().ToLower())).ToList();
            }
            return bookings != null ?
                          View(bookings) :
                          Problem("Entity set 'HotelManagementContext.Bookings'  is null.");
        }

        // GET: Bookings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Bookings == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings
                .FirstOrDefaultAsync(m => m.Id == id);
            if (booking == null)
            {
                return NotFound();
            }
            var availableRooms = _context.RoomBookings.Include(x => x.Room).Where(x => x.BookingId == id).ToList();
            TempData["AvailableRooms"] = availableRooms;
            return View(booking);
        }

        // GET: Bookings/Create
        public IActionResult Create()
        {
            var availableRooms = _context.Rooms.Where(x => x.RoomStatus == RoomStatus.Empty).ToList();
            TempData["AvailableRooms"] = availableRooms;

            return View();
        }

        // POST: Bookings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Checkin,Checkout,BookingStatus,Note,CustomerName,CustomerPhone,PayMethod")] Booking booking, int selectedRoom)
        {
            if (ModelState.IsValid && selectedRoom > 0)
            {
                var room = await _context.Rooms.FirstOrDefaultAsync(x => x.Id == selectedRoom);
                var roomBooking = new RoomBooking { RoomId = selectedRoom };
                var transaction = new Transaction
                {
                    Total = room.Price,
                    TransactionTime = DateTime.Now,
                    IsPaid = booking.BookingStatus,
                    CustomerName = booking.CustomerName
                };
                room.RoomStatus = RoomStatus.Booked;
                booking.RoomBookings.Add(roomBooking);
                booking.Transaction = transaction;
                var roomTransaction = new RoomTransaction { RoomId = selectedRoom };
                transaction.RoomTransactions.Add(roomTransaction);
                _context.Add(booking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = booking.Id });
            }
            return View(booking);
        }

        // GET: Bookings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (id == null || _context.Bookings == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }
            var availableRooms = _context.RoomBookings.Include(x => x.Room).Where(x => x.BookingId == id).ToList();
            TempData["AvailableRooms"] = availableRooms;
            return View(booking);
        }

        // POST: Bookings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Checkin,Checkout,BookingStatus,Note,CustomerName,CustomerPhone,PayMethod")] Booking booking)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (id != booking.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(booking);
        }

        // GET: Bookings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (id == null || _context.Bookings == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings
                .FirstOrDefaultAsync(m => m.Id == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (_context.Bookings == null)
            {
                return Problem("Entity set 'HotelManagementContext.Bookings'  is null.");
            }
            var booking = await _context.Bookings.FindAsync(id);
            if (booking != null)
            {
                _context.Bookings.Remove(booking);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookingExists(int id)
        {
            return (_context.Bookings?.Any(e => e.Id == id)).GetValueOrDefault();
        }
        public IActionResult GetRoomDetails(int selectedRoom)
        {
            if (selectedRoom > 0)
            {
                var room = _context.Rooms.FirstOrDefault(x => x.Id == selectedRoom);
                return Json(room);
            }
            var emptyRoom = new Room();
            return Json(emptyRoom);
        }
    }
}
