﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using hotel_management_mvc.Models;
using Newtonsoft.Json;
using static System.TimeZoneInfo;

namespace hotel_management_mvc.Controllers
{
    public class TransactionsController : Controller
    {
        private readonly HotelManagementContext _context;

        public TransactionsController(HotelManagementContext context)
        {
            _context = context;
        }

        // GET: Transactions
        public async Task<IActionResult> Index(string searchString)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            var transaction = await _context.Transactions.OrderByDescending(x => x.Id).ToListAsync();
            if (!string.IsNullOrEmpty(searchString))
            {
                transaction = transaction.Where(x => x.CustomerName.Trim().ToLower().Contains(searchString.Trim().ToLower()) || x.BookingId.ToString().Trim().ToLower().Contains(searchString.Trim().ToLower()) || x.IsPaid.ToString().Trim().ToLower().Contains(searchString.Trim().ToLower())).ToList();
            }
            return transaction != null ?
                          View(transaction) :
                          Problem("Entity set 'HotelManagementContext.transaction'  is null.");
        }

        public IActionResult GetIncome(DateTime? startDate, DateTime? endTime)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            var transaction = _context.Transactions.Where(x => x.IsPaid == BookingStatus.Paid).Sum(x => x.Total);
            if (startDate != null && endTime != null)
            {
                transaction = _context.Transactions.Where(x => x.IsPaid == BookingStatus.Paid && x.TransactionTime >= startDate && x.TransactionTime <= endTime).Sum(x => x.Total);
            }
            return Json(transaction);
        }

        // GET: Transactions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Transactions == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transactions
                .Include(t => t.Booking)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transaction == null)
            {
                return NotFound();
            }
            var availableRooms = _context.RoomTransactions.Include(x => x.Room).Where(x => x.TransactionId == id).ToList();
            TempData["AvailableRooms"] = availableRooms;

            return View(transaction);
        }

        // GET: Transactions/Create
        public IActionResult Create()
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            ViewData["BookingId"] = new SelectList(_context.Bookings, "Id", "Id");
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Total,TransactionTime,IsPaid,Note,CustomerName,BookingId")] Transaction transaction)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (ModelState.IsValid)
            {
                _context.Add(transaction);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookingId"] = new SelectList(_context.Bookings, "Id", "Id", transaction.BookingId);
            return View(transaction);
        }

        // GET: Transactions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (id == null || _context.Transactions == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transactions.FindAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }
            var availableRooms = _context.RoomTransactions.Include(x => x.Room).Where(x => x.TransactionId == id).ToList();
            TempData["AvailableRooms"] = availableRooms;
            ViewData["BookingId"] = new SelectList(_context.Bookings, "Id", "Id", transaction.BookingId);
            return View(transaction);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Total,TransactionTime,IsPaid,Note,CustomerName,BookingId")] Transaction transaction)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (id != transaction.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(transaction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransactionExists(transaction.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookingId"] = new SelectList(_context.Bookings, "Id", "Id", transaction.BookingId);
            return View(transaction);
        }

        // GET: Transactions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (id == null || _context.Transactions == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transactions
                .Include(t => t.Booking)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transaction == null)
            {
                return NotFound();
            }

            return View(transaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (_context.Transactions == null)
            {
                return Problem("Entity set 'HotelManagementContext.Transactions'  is null.");
            }
            var transaction = await _context.Transactions.FindAsync(id);
            if (transaction != null)
            {
                _context.Transactions.Remove(transaction);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TransactionExists(int id)
        {
            return (_context.Transactions?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
