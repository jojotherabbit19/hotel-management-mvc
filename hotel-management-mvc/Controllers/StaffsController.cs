﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using hotel_management_mvc.Models;
using Newtonsoft.Json;

namespace hotel_management_mvc.Controllers
{
    public class StaffsController : Controller
    {
        private readonly HotelManagementContext _context;

        public StaffsController(HotelManagementContext context)
        {
            _context = context;
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel userModel)
        {
            if (ModelState.IsValid)
            {
                var user = await _context.Users.FirstOrDefaultAsync(x => x.UserName == userModel.UserName && x.Password == userModel.Password);
                if (user != null)
                {
                    var authen = new AuthenModel
                    {
                        Id = user.Id,
                        Role = user.Role.ToString(),
                    };
                    var value = JsonConvert.SerializeObject(authen);
                    HttpContext.Session.SetString("role", value);
                    return RedirectToAction(nameof(Index), "Rooms");
                }
            }
            return RedirectToAction(nameof(Login));
        }
        // GET: Staffs
        public async Task<IActionResult> Index(string searchString)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            var users = await _context.Users.ToListAsync();
            if (!string.IsNullOrEmpty(searchString))
            {
                users = users.Where(x => x.UserName.Trim().ToLower().Contains(searchString.Trim().ToLower()) || x.FullName.Trim().ToLower().Contains(searchString.Trim().ToLower())).ToList();
            }
            return users != null ?
                          View(users) :
                          Problem("Entity set 'HotelManagementContext.Staffs'  is null.");
        }

        // GET: Staffs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            if (id == null || _context.Users == null)
            {
                return NotFound();
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Staffs/Create
        public IActionResult Create()
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            return View();
        }

        // POST: Staffs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserName,Password,FullName,Phone,Address,Role,Position,AccountStatus")] User user)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (ModelState.IsValid)
            {
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Staffs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (id == null || _context.Users == null)
            {
                return NotFound();
            }

            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Staffs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserName,Password,FullName,Phone,Address,Role,Position,AccountStatus")] User user)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Staffs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (id == null || _context.Users == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Staffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (_context.Users == null)
            {
                return Problem("Entity set 'HotelManagementContext.Users'  is null.");
            }
            var user = await _context.Users.FindAsync(id);
            if (user != null)
            {
                _context.Users.Remove(user);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return (_context.Users?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
