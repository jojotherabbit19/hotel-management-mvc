﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using hotel_management_mvc.Models;
using Newtonsoft.Json;

namespace hotel_management_mvc.Controllers
{
    public class RoomsController : Controller
    {
        private readonly HotelManagementContext _context;

        public RoomsController(HotelManagementContext context)
        {
            _context = context;
            UpdateRoomStatus();
        }

        // GET: Rooms
        public async Task<IActionResult> Index(string searchString)
        {
            var room = await _context.Rooms.ToListAsync();
            if (!string.IsNullOrEmpty(searchString))
            {
                room = room.Where(x => x.Code.Trim().ToLower().Contains(searchString.Trim().ToLower()) || x.Services.Trim().ToLower().Contains(searchString.Trim().ToLower())).ToList();
            }
            return room != null ?
                          View(room) :
                          Problem("Entity set 'HotelManagementContext.Rooms'  is null.");
        }

        // GET: Rooms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Rooms == null)
            {
                return NotFound();
            }

            var room = await _context.Rooms
                .FirstOrDefaultAsync(m => m.Id == id);
            if (room == null)
            {
                return NotFound();
            }

            return View(room);
        }

        // GET: Rooms/Create
        public IActionResult Create()
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            return View();
        }

        // POST: Rooms/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Code,Price,Services")] Room room)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (ModelState.IsValid)
            {
                room.RoomStatus = RoomStatus.Empty;
                _context.Add(room);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(room);
        }

        // GET: Rooms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (id == null || _context.Rooms == null)
            {
                return NotFound();
            }

            var room = await _context.Rooms.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }
            return View(room);
        }

        // POST: Rooms/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Code,Price,Services,RoomStatus")] Room room)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (id != room.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(room);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoomExists(room.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(room);
        }

        // GET: Rooms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (id == null || _context.Rooms == null)
            {
                return NotFound();
            }

            var room = await _context.Rooms
                .FirstOrDefaultAsync(m => m.Id == id);
            if (room == null)
            {
                return NotFound();
            }

            return View(room);
        }

        // POST: Rooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var value = HttpContext.Session.GetString("role");
            if (value == null)
            {
                return View("Error");
            }
            var role = JsonConvert.DeserializeObject<AuthenModel>(value);
            if (role.Role != "Admin")
            {
                return View("Error");
            }
            if (_context.Rooms == null)
            {
                return Problem("Entity set 'HotelManagementContext.Rooms'  is null.");
            }
            var room = await _context.Rooms.FindAsync(id);
            if (room != null)
            {
                _context.Rooms.Remove(room);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RoomExists(int id)
        {
            return (_context.Rooms?.Any(e => e.Id == id)).GetValueOrDefault();
        }
        private void UpdateRoomStatus()
        {
            var rooms = _context.Rooms.Include(x => x.RoomBookings).ThenInclude(x => x.Booking).Where(x => x.RoomStatus == RoomStatus.Booked).ToList();
            foreach (var room in rooms)
            {
                foreach (var item in room.RoomBookings)
                {
                    if (item.Booking.Checkout <= DateTime.Now)
                    {
                        room.RoomStatus = RoomStatus.Empty;
                    }
                }
            }
            _context.UpdateRange(rooms);
            _context.SaveChanges();
        }
    }
}
