﻿using System;
using System.Collections.Generic;

namespace hotel_management_mvc.Models;

public partial class RoomBooking
{
    public int Id { get; set; }

    public int RoomId { get; set; }

    public int BookingId { get; set; }

    public virtual Booking Booking { get; set; } = null!;

    public virtual Room Room { get; set; } = null!;
}
