﻿using System;
using System.Collections.Generic;

namespace hotel_management_mvc.Models;

public partial class User
{
    public int Id { get; set; }

    public string UserName { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string? FullName { get; set; }

    public string? Phone { get; set; }

    public string? Address { get; set; }

    public Role Role { get; set; }

    public string Position { get; set; } = null!;

    public AccountStatus AccountStatus { get; set; }
}
