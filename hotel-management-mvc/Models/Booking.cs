﻿using System;
using System.Collections.Generic;

namespace hotel_management_mvc.Models;

public partial class Booking
{
    public int Id { get; set; }

    public DateTime Checkin { get; set; }

    public DateTime Checkout { get; set; }

    public BookingStatus BookingStatus { get; set; }

    public string? Note { get; set; }

    public string CustomerName { get; set; } = null!;

    public string CustomerPhone { get; set; } = null!;

    public PayMethod PayMethod { get; set; }

    public virtual ICollection<RoomBooking> RoomBookings { get; set; } = new List<RoomBooking>();

    public virtual Transaction? Transaction { get; set; }
}
