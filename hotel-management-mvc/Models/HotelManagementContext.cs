﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace hotel_management_mvc.Models;

public partial class HotelManagementContext : DbContext
{
    public HotelManagementContext()
    {
    }

    public HotelManagementContext(DbContextOptions<HotelManagementContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Booking> Bookings { get; set; }

    public virtual DbSet<Room> Rooms { get; set; }

    public virtual DbSet<RoomBooking> RoomBookings { get; set; }

    public virtual DbSet<RoomTransaction> RoomTransactions { get; set; }

    public virtual DbSet<Transaction> Transactions { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=JOJOTHERABBIT\\JOJO;Initial Catalog=hotel-management;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Room>(entity =>
        {
            entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");
        });

        modelBuilder.Entity<RoomBooking>(entity =>
        {
            entity.HasIndex(e => e.BookingId, "IX_RoomBookings_BookingId");

            entity.HasIndex(e => e.RoomId, "IX_RoomBookings_RoomId");

            entity.HasOne(d => d.Booking).WithMany(p => p.RoomBookings).HasForeignKey(d => d.BookingId);

            entity.HasOne(d => d.Room).WithMany(p => p.RoomBookings).HasForeignKey(d => d.RoomId);
        });

        modelBuilder.Entity<RoomTransaction>(entity =>
        {
            entity.HasIndex(e => e.RoomId, "IX_RoomTransactions_RoomId");

            entity.HasIndex(e => e.TransactionId, "IX_RoomTransactions_TransactionId");

            entity.HasOne(d => d.Room).WithMany(p => p.RoomTransactions).HasForeignKey(d => d.RoomId);

            entity.HasOne(d => d.Transaction).WithMany(p => p.RoomTransactions).HasForeignKey(d => d.TransactionId);
        });

        modelBuilder.Entity<Transaction>(entity =>
        {
            entity.HasIndex(e => e.BookingId, "IX_Transactions_BookingId").IsUnique();

            entity.Property(e => e.Total).HasColumnType("decimal(18, 2)");

            entity.HasOne(d => d.Booking).WithOne(p => p.Transaction).HasForeignKey<Transaction>(d => d.BookingId);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
