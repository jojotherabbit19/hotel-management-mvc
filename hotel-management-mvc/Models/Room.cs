﻿using System;
using System.Collections.Generic;

namespace hotel_management_mvc.Models;

public partial class Room
{
    public int Id { get; set; }

    public string Code { get; set; } = null!;

    public decimal Price { get; set; }

    public string? Services { get; set; }

    public RoomStatus RoomStatus { get; set; }

    public virtual ICollection<RoomBooking> RoomBookings { get; set; } = new List<RoomBooking>();

    public virtual ICollection<RoomTransaction> RoomTransactions { get; set; } = new List<RoomTransaction>();
}
