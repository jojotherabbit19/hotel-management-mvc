﻿namespace hotel_management_mvc.Models
{
    public enum Role
    {
        Admin = 1,
        Staff = 2,
        Customer = 3
    }
    public enum AccountStatus
    {
        Active = 1,
        Inactive = 2
    }
    public enum RoomStatus
    {
        Empty = 1,
        Booked = 2,
        UnderConstruction = 3
    }
    public enum BookingStatus
    {
        Paid = 1,
        Unpaid = 2
    }
    public enum PayMethod
    {
        Cash = 1,
        Card = 2
    }
}
