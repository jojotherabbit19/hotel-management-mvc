﻿using System;
using System.Collections.Generic;

namespace hotel_management_mvc.Models;

public partial class Transaction
{
    public int Id { get; set; }

    public decimal Total { get; set; }

    public DateTime TransactionTime { get; set; }

    public BookingStatus IsPaid { get; set; }

    public string? Note { get; set; }

    public string CustomerName { get; set; } = null!;

    public int BookingId { get; set; }

    public virtual Booking? Booking { get; set; } = null!;

    public virtual ICollection<RoomTransaction> RoomTransactions { get; set; } = new List<RoomTransaction>();
}
