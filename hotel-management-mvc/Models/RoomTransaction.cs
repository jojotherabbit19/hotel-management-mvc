﻿using System;
using System.Collections.Generic;

namespace hotel_management_mvc.Models;

public partial class RoomTransaction
{
    public int Id { get; set; }

    public int RoomId { get; set; }

    public int TransactionId { get; set; }

    public virtual Room Room { get; set; } = null!;

    public virtual Transaction Transaction { get; set; } = null!;
}
